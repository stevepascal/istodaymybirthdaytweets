"""
    Determines if today is my Birthday and tweet the result.
"""
from datetime import datetime, date
import tweepy

# My Birthday
BIRTH_YEAR = 1989
BIRTH_MONTH = 6
BIRTH_DATE = 4

# Twitter API credentials
CONSUMER_KEY = 'redacted'
CONSUMER_SECRET = 'redacted'
ACCESS_TOKEN = 'redacted'
ACCESS_TOKEN_SECRET = 'redacted'


def days_until_birthday():
    """
        Determines how many days until next birthday
    """
    now = date.today().toordinal()
    current_year = datetime.now().year
    upcoming_birthday = date(current_year, BIRTH_MONTH, BIRTH_DATE).toordinal()
    day_until = upcoming_birthday - now

    # if birthday already passed in current year, need to recalculate for next years

    if day_until < 0:
        upcoming_birthday = date(current_year + 1, BIRTH_MONTH, BIRTH_DATE).toordinal()
        day_until = upcoming_birthday - now

    return day_until


def get_age_in_years():

    """
        returns age in year
    """
    current_year = datetime.now().year
    return current_year - BIRTH_YEAR


def tweet_it(text: str):
    """
        Tweet a given text.
    """
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
    api = tweepy.API(auth)
    api.update_status(text)


def launch():
    """
        Starts the program. The beginning.
    """
    remaining_days = days_until_birthday()
    text_to_tweet = f"Today is {datetime.today().strftime('%m/%d/%Y')}"
    if remaining_days > 1 :
        text_to_tweet += f" and there are {remaining_days} days until my birthday!"
    elif remaining_days == 1 :
        text_to_tweet += " and tomorrow is my birthday!"
    elif remaining_days == 0 :
        age_in_years = get_age_in_years()
        text_to_tweet += f" and it is my birthday. I am now {age_in_years} years old."

    # print(text_to_tweet)
    tweet_it(text_to_tweet)


if __name__ == "__main__":
    launch()

